package Lexer;

import java.util.List;

public class Token {
    public static enum TokenType {
        CHAR,
        OPENADT,
        CLOSEADT,
        OPENCALL,
        CLOSECALL,
        OPENBLOCK,
        CLOSEBLOCK,
        OPENBRACKET,
        CLOSEBRASCET,
        COMMA,
        DIRECTIVE,
        IDENTMARKER,
        NAME,
        NUMBER,
        REPLACE,
        SEMICOLON,
        VARIABLE,
        REDEFENITION,
        EOF,
        UNEXPECTED,
        OP
    }

    private TokenType tokenType;
    private String value;
    private List<Token> parenTokens;
    private List<Token> child;

    public Token(TokenType type, String value) {
        tokenType = type;
        this.value = value;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Token> getParenTokens() {
        return parenTokens;
    }

    public List<Token> getChild() {
        return child;
    }
}
