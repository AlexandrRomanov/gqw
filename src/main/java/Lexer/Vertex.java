package Lexer;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
    private Token token;
    private List<Vertex> childrens;
    private Vertex parent;

    public Vertex(Token token) {
        this.token = token;
        childrens = new ArrayList<Vertex>();
        parent = null;
    }
}
