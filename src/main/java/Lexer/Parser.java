package Lexer;

import java.util.ArrayList;
import java.util.List;

public class Parser {

    private List<Token> tokenList = new ArrayList<Token>();
    private int counter = 0;
    private String program;

    public Parser(String program) {
//        Token currentToken;
//        do {
//            currentToken = nextToken(program);
//            tokenList.add(currentToken);
//        }
//        while (currentToken.getTokenType() != Token.TokenType.EOF);
        this.program = program;
    }

    public Token nextToken() {
        while (counter != program.length() && (program.charAt(counter) == '\n' || program.charAt(counter) == ' ')) {
            counter++;
        }
        if (counter == program.length()) {
            return new Token(Token.TokenType.EOF, "\n");
        }
        if(program.charAt(counter) >= '0' && program.charAt(counter) <= '9') {
            int begin = counter;
            while (program.charAt(counter) >= '0' && program.charAt(counter) <= '9') {
                counter++;
            }
            return new Token(Token.TokenType.NUMBER, program.substring(begin, counter));
        }
        if (program.charAt(counter) == '$') {
            counter++;
            if (program.indexOf("EENUM", counter) == counter + 5) {
                counter += 5;
                return new Token(Token.TokenType.DIRECTIVE, program.substring(counter - 5, counter));
            }
            if (program.indexOf("ENTRY", counter) == counter + 5) {
                counter += 5;
                return new Token(Token.TokenType.DIRECTIVE, program.substring(counter - 5, counter));
            }
            if (program.indexOf("ENUM", counter) == counter + 4) {
                counter += 4;
                return new Token(Token.TokenType.DIRECTIVE, program.substring(counter - 4, counter));
            }
            if (program.indexOf("EXTERN", counter) == counter + 6) {
                counter += 6;
                return new Token(Token.TokenType.DIRECTIVE, program.substring(counter - 6, counter));
            }
            if (program.indexOf("FORWARD", counter) == counter + 7) {
                counter += 7;
                return new Token(Token.TokenType.DIRECTIVE, program.substring(counter - 7, counter));
            }
            if (program.indexOf("SWAP", counter) == counter + 4) {
                counter += 4;
                return new Token(Token.TokenType.DIRECTIVE, program.substring(counter - 4, counter));
            }
            if (program.indexOf("ESWAP", counter) == counter + 5) {
                counter += 5;
                return new Token(Token.TokenType.DIRECTIVE, program.substring(counter - 5, counter));
            }
            if (program.indexOf("LABEL", counter) == counter + 5) {
                counter += 5;
                return new Token(Token.TokenType.DIRECTIVE, program.substring(counter - 5, counter));
            }
        }
        if(program.charAt(counter) >= 'a' && program.charAt(counter) <= 'z') {
            int begin = counter;
            while (program.charAt(counter) >= 'a' && program.charAt(counter) <= '9') {
                counter++;
            }
            return new Token(Token.TokenType.NUMBER, program.substring(begin, counter));
        }
        switch (program.charAt(counter)) {
            case '\'':
                counter += 3;
                return new Token(Token.TokenType.CHAR, program.substring(counter - 2, counter - 1));
            case '#':
                counter++;
                return new Token(Token.TokenType.IDENTMARKER, "#");
            case '[':
                counter++;
                return new Token(Token.TokenType.OPENADT, "[");
            case ']':
                counter++;
                return new Token(Token.TokenType.CLOSEADT, "]");
            case '<':
                counter++;
                return new Token(Token.TokenType.OPENCALL, "<");
            case '>':
                counter++;
                return new Token(Token.TokenType.CLOSECALL, ">");
            case '{':
                counter++;
                return new Token(Token.TokenType.OPENBLOCK, "{");
            case '}':
                counter++;
                return new Token(Token.TokenType.CLOSEBLOCK, "}");
            case '(':
                counter++;
                return new Token(Token.TokenType.OPENBRACKET, "(");
            case ')':
                counter++;
                return new Token(Token.TokenType.CLOSEBRASCET, ")");
            case ',':
                counter++;
                return new Token(Token.TokenType.COMMA, ",");
            case '=':
                counter++;
                return new Token(Token.TokenType.REPLACE, "=");
            case ';':
                counter++;
                return new Token(Token.TokenType.SEMICOLON, ";");
            case '^':
                counter++;
                return new Token(Token.TokenType.REDEFENITION, "^");
            case '+':
                counter++;
                return new Token(Token.TokenType.OP, "+");
            case '-':
                counter++;
                return new Token(Token.TokenType.OP, "-");
            case '*':
                counter++;
                return new Token(Token.TokenType.OP, "*");
            case '/':
                counter++;
                return new Token(Token.TokenType.OP, "/");
        }
        counter++;
        return new Token(Token.TokenType.UNEXPECTED, "");
    }

    public void parsing() {

    }
}
